# coding=utf-8
from flask import Flask, jsonify, request, send_from_directory
from model.dllLoader import WinFunction
from model.tools import ReceiptDatetime
app = Flask(__name__, static_url_path='/static')
dll = WinFunction()


@app.route('/404')
def page404():
    return send_from_directory('template/', '404.html')


@app.route('/')
def index():
    return '<form method="post" action="/print">' \
           '<input type="submit" value="print" />' \
           '</form>'


@app.route('/print', methods=['POST'])
def print_page():
    httpRequest = request.get_json()
    bar_code = str(httpRequest['bar_code'])  # "10710JA195288030822"
    QR_code_left = str(httpRequest['QR_code_left'])  # "JA1952880310710250822000005960000059600000000546594733r69ryUM0ZtCU5HR1giNnA==:**********:1:1:1:D69485:1:1430"
    QR_code_right = str(httpRequest['QR_code_right'])  # "**                                                                                                            "
    isReprint = bool(httpRequest['isReprint'])  # True
    iid_str = str(httpRequest['iid'])  # "JA-19528803"
    time = ReceiptDatetime(str(httpRequest['datetime']), '%Y-%m-%d %H:%M:%S')  # "2018-10-25 13:32:04"
    isFormat25 = bool(httpRequest('isFormat25'))  # True
    strPCKey = str(httpRequest('PCKey'))  # "0822"
    DMey = str(httpRequest('IMey'))  # "1430"
    seller = str(httpRequest('seller'))  # "54659473"
    store_name = str(httpRequest('store_name'))  # "悠跑國際有限公司流行分公司"
    store_tel = str(httpRequest('store_tel'))  # "12345678"
    str_footer = str(httpRequest('footer'))  # "退換貨請帶發票證明聯及交易明細辦理"

    font = '細明體'
    fontStyle = 0
    datetime_str = time.toString('%Y-%m-%d %H:%M:%S')
    month_str = time.getMonthString()

    dll.open('SLK-TL120', 1)
    dll.print_start()
    dll.set_master_unit(0)
    dll.set_width(404)
    dll.set_label_size(404, 200)
    dll.print_bitmap('static/dll/LKView.bmp', 0, 0, 10, 0)
    if isReprint:
        dll.font_and_coordinate(0, 0, font, fontStyle, 38, '電子發票證明聯', 0)
        dll.font_and_coordinate(270, 10, font, fontStyle, 28, '(補印)', 0)
    else:
        dll.font_and_coordinate(35, 0, font, fontStyle, 38, '電子發票證明聯', 0)
    dll.font_and_coordinate(18, 35, "Arial", fontStyle, 52, month_str, 0)
    dll.font_and_coordinate(36, 82, "Arial", fontStyle, 52, iid_str, 0)
    if isFormat25:
        datetime_str = datetime_str + '   格式 25  '
    dll.font_and_coordinate(14, 125, font, fontStyle, 22, datetime_str, 0)
    dll.font_and_coordinate(14, 147, font, fontStyle, 22, '隨機碼  ' + strPCKey + '     總計' + DMey, 0)
    dll.font_and_coordinate(14, 169, font, fontStyle, 22, '賣方 ' + seller + '    ', 0)
    dll.print_label()
    dll.print_bar_code(bar_code, 109, 40, 404, 0, 0)
    dll.set_label_size(404, 180)
    dll.print_qr_code_xy(14, 10, QR_code_left, 0, 3, 0, 6, -1)
    dll.print_qr_code_xy(219, 10, QR_code_right, 0, 3, 0, 6, -1)
    dll.font_and_coordinate(14, 138, font, fontStyle, 20, '門市  ' + store_name + 'TEL ' + store_tel, 0)
    dll.font_and_coordinate(14, 160, font, fontStyle, 20, str_footer, 0)
    dll.print_label()
    dll.cut()
    dll.print_stop()
    dll.close()
    return '列印成功'


# def printDetail():


if __name__ == '__main__':
    app.run(threaded=True, host='0.0.0.0', port='7100')
