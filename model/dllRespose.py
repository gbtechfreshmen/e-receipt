from enum import Enum


class OpenPortRes(Enum):
    LK_SUCCESS = 0  # returns after the function success
    LK_CREATE_ERROR = 1  # failure to create communication target
    LK_NOT_OPENED = 2  # returns when unknown printer has been connected


class ClosePortRes(Enum):
    LK_SUCCESS = 0  # returns after function success
    LK_NOT_OPENED = 1  # the communication port is closed


class Message(Enum):
    LK_SUCCESS = 0  # returns after function success
    UNKNOWN = 1


class PrinterStsRes(Enum):
    LK_STS_NORMAL = 0  # Success
    LK_STS_COVEROPEN = 1  # Cover Open ERROR
    LK_STS_PAPEREMPTY = 2  # Paper Empty ERROR
    LK_STS_POWEROFF = 3  # Power OFF ERROR
    LK_STS_PAPERNEAREMPTY = 4  # Paper Near Empty Warning
