from ctypes import *
from model.dllRespose import *


class WinFunction:

    def __init__(self):
        file_name = 'static/dll/LKPOSTOT.dll'
        self.func = windll.LoadLibrary(file_name)
        self.func_setup()

    def func_setup(self):
        dll_fun = self.func
        dll_fun.OpenPort.argtypes = [c_char_p, c_long]
        dll_fun.OpenPort.restype = c_long
        dll_fun.ClosePort.restype = c_long
        dll_fun.PrintStart.restype = c_long
        dll_fun.PrintStop.restype = c_long
        dll_fun.CutPaper.restype = c_long
        dll_fun.PrinterSts.restype = c_long
        dll_fun.PrintLabel.restype = c_long
        dll_fun.PrintingWidth.restype = c_long
        dll_fun.SetMasterUnit.argtypes = [c_long]
        dll_fun.SetMasterUnit.restype = c_long
        dll_fun.SetLabelSize.argtypes = [c_long, c_long]
        dll_fun.SetMasterUnit.restype = c_long
        dll_fun.SetLabelSize.restype = c_long
        dll_fun.PrintTTFAlign.restype = c_long
        dll_fun.PrintTTFXY.restype = c_long
        dll_fun.PrintBarCode.restype = c_long
        dll_fun.PrintQRCodeAlign.restype = c_long
        dll_fun.PrintQRCodeXY.restype = c_long
        dll_fun.PrintNormal.restype = c_long
        dll_fun.PrintBitmapAlign.restype = c_long
        dll_fun.OpenDrawer.restype = c_long
        dll_fun.PrintBitmap.restype = c_long

    def open(self, port_name, number):
        # frame = inspect.currentframe()
        # args, _, _, values = inspect.getargvalues(frame)
        sz_port_name = c_char_p(port_name.encode('big5'))
        baud_rate = c_long(number)
        res = OpenPortRes(self.func.OpenPort(sz_port_name, baud_rate))
        print ('[open] ' + res.name)
        return res

    def close(self):
        res = ClosePortRes(self.func.ClosePort())
        print ('[close] ' + res.name)
        return res

    def print_start(self):
        self.func.PrintStart()
        print ("[start] now starting...")

    def print_stop(self):
        self.func.PrintStop()
        print ("[stop] stop printing.")

    def cut(self):
        res = Message(self.func.CutPaper())
        self.success(res, "cut paper")
        return res

    def status(self):
        res = PrinterStsRes(self.func.PrinterSts())
        print ("[status] " + res.name)
        return res

    def blank(self):
        res = Message(self.func.PrintLabel())
        self.success(res, "print lable")
        return res

    def set_width(self, length):
        width = c_long(length)
        res = Message(self.func.PrintingWidth(width))
        self.success(res, "width set to " + str(length))
        return res

    def set_master_unit(self, unit):
        unit_measure = c_long(unit)
        res = Message(self.func.SetMasterUnit(unit_measure))
        self.success(res, "unit set to " + str(unit))
        return res

    def set_label_size(self, width, height):
        width_size = c_long(width)
        height_size = c_long(height)
        res = Message(self.func.SetLabelSize(width_size, height_size))
        self.success(res, "label size set to " + str(width) + " * " + str(height))
        return res

    def font(self, alignment_, base_unity, font_name, font_style, font_do_size, text_data, reverse_print):
        alignment_ = c_long(alignment_)
        base_unity = c_long(base_unity)
        font_name = c_char_p(font_name.encode('big5'))
        font_style = c_long(font_style)
        font_do_size = c_long(font_do_size)
        text_data = c_char_p(text_data.encode('big5'))
        reverse_print = c_long(reverse_print)
        res = Message(self.func.PrintTTFAlign(alignment_, base_unity, font_name
                                              , font_style, font_do_size, text_data, reverse_print))
        self.success(res, "")
        return res

    def font_and_coordinate(self, base_unit_x, base_unit_y, font_name
                            , font_style, font_dot_size, text_data, reverse_print):
        base_unit_x = c_long(base_unit_x)
        base_unit_y = c_long(base_unit_y)
        font_name = c_char_p(font_name.encode('big5'))
        font_style = c_long(font_style)
        font_dot_size = c_long(font_dot_size)
        text_data = c_char_p(text_data.encode('big5'))
        reverse_print = c_long(reverse_print)
        res = Message(self.func.PrintTTFXY(base_unit_x, base_unit_y, font_name, font_style
                                           , font_dot_size, text_data, reverse_print))
        self.success(res, "")
        return res

    def print_bar_code(self, data, symbology, height, width, alignment_, text_position):
        data = c_char_p(data.encode('big5'))
        symbology = c_long(symbology)
        height = c_long(height)
        width = c_long(width)
        alignment_ = c_long(alignment_)
        text_position = c_long(text_position)
        res = Message(self.func.PrintBarCode(data, symbology, height, width, alignment_, text_position))
        self.success(res, "")
        return res

    def print_qr_code_align(self, alignment_, base_unit_y, data, size, module_size, ec_level, version, mask_pattern):
        alignment_ = c_long(alignment_)
        base_unit_y = c_long(base_unit_y)
        data = c_char_p(data.encode('big5'))
        size = c_long(size)
        module_size = c_long(module_size)
        ec_level = c_long(ec_level)
        version = c_long(version)
        mask_pattern = c_long(mask_pattern)
        res = Message(self.func.PrintQRCodeAlign(alignment_, base_unit_y, data, size
                                                 , module_size, ec_level, version, mask_pattern))
        self.success(res, "")
        return res

    def print_qr_code_xy(self, base_unit_x, base_unit_y, data, size, module_size, ec_level, version, mask_pattern):
        base_unit_x = c_long(base_unit_x)
        base_unit_y = c_long(base_unit_y)
        data = c_char_p(data.encode('big5'))
        size = c_long(size)
        module_size = c_long(module_size)
        ec_level = c_long(ec_level)
        version = c_long(version)
        mask_pattern = c_long(mask_pattern)
        res = Message(self.func.PrintQRCodeXY(base_unit_x, base_unit_y, data, size
                                              , module_size, ec_level, version, mask_pattern))
        self.success(res, "")
        return res

    def print_normal(self, data):
        data = c_char_p(data.encode('big5'))
        res = Message(self.func.PrintNormal(data))
        self.success(res, "")
        return res

    def print_bitmap_align(self, alignment_, base_unit_y, bitmap_file):
        alignment_ = c_long(alignment_)
        base_unit_y = c_long(base_unit_y)
        bitmap_file = c_byte(bitmap_file)
        res = Message(self.func.PrintBitmapAlign(alignment_, base_unit_y, bitmap_file))
        self.success(res, "")
        return res

    def open_drawer(self, drawer_pin_num, pulse_on_time, pulse_off_time):
        drawer_pin_num = c_long(drawer_pin_num)
        pulse_on_time = c_long(pulse_on_time)
        pulse_off_time = c_long(pulse_off_time)
        res = Message(self.func.OpenDrawer(drawer_pin_num, pulse_on_time, pulse_off_time))
        self.success(res, "")
        return res

    # print image
    def print_bitmap(self, bitmap_name, alignment_, options, brightness, image_mode):
        bitmap_name = c_char_p(bitmap_name.encode('big5'))
        alignment_ = c_long(alignment_)
        options = c_long(options)
        brightness = c_long(brightness)
        image_mode = c_long(image_mode)
        res = Message(self.func.PrintBitmap(bitmap_name, alignment_, options, brightness, image_mode))
        self.success(res, "")
        return res

    def print_label(self):
        self.func.PrintLabel()
        print("[print] now printing")

    @staticmethod
    def success(res, message):
        if res == Message.LK_SUCCESS:
            print (message + " success")
        else:
            print (message + " failed")

    @staticmethod
    def change_types_to_c(param):
        py_type = type(param)
        if py_type == int:
            param = c_long(param)
        elif py_type == str:
            param = c_char_p(param.encode('big5'))
        return param


