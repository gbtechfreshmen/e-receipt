# coding=utf-8
from datetime import datetime


class ReceiptDatetime:

    def __init__(self, date_string, _format):
        self.datetime = datetime.strptime(date_string=date_string, format=_format)

    def getMonthString(self):  # "107年09-10月"
        year = str(self.datetime.year - 1911)
        upperBound = self.toMonthString(self.getMonthUpperBound())
        lowBound = self.toMonthString(self.getMonthLowBound())
        return year + '年' + lowBound + '-' + upperBound + '月'

    def toString(self, _format):
        return self.datetime.strftime(_format)

    def getMonthUpperBound(self):
        month = self.datetime.month
        if month % 2 == 0:
            return month
        else:
            return month + 1

    def getMonthLowBound(self):
        month = self.datetime.month
        if month % 2 == 0:
            return month - 1
        else:
            return month

    @staticmethod
    def toMonthString(number):
        MONTH_STRING_LENGTH = 2
        return str(number).zfill(width=MONTH_STRING_LENGTH)
