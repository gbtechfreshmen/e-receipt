Attribute VB_Name = "QRCode"
'開post(發送端名稱(印表機名稱),開啟端口(post)) 回傳值 成功連接QRCodePrintInUnite
Public Declare Function OpenPort Lib "LKPOSTOT.DLL" (ByVal PortName As String, ByVal BaudRate As Long) As Long
'關post 回傳值 LK_SUCCESS 成功 LK_NOT_OPENED 以關閉
Public Declare Function ClosePort Lib "LKPOSTOT.DLL" () As Long
'切紙
Private Declare Function CutPaper Lib "LKPOSTOT.DLL" () As Long
'開始
Private Declare Function PrintStart Lib "LKPOSTOT.DLL" () As Long
'狀態
Private Declare Function PrinterSts Lib "LKPOSTOT.DLL" () As Long
'停止
Private Declare Function PrintStop Lib "LKPOSTOT.DLL" () As Long
'空行
Private Declare Function PrintLabel Lib "LKPOSTOT.DLL" () As Long
'紙張寬
Private Declare Function PrintingWidth Lib "LKPOSTOT.DLL" (ByVal pwidth As Long) As Long
'單位
Private Declare Function SetMasterUnit Lib "LKPOSTOT.DLL" (ByVal UnitMeasure As Long) As Long
'格大小
Private Declare Function SetLabelSize Lib "LKPOSTOT.DLL" (ByVal widthsize As Long, ByVal heightsize As Long) As Long
'字體樣式
Private Declare Function PrintTTFAlign Lib "LKPOSTOT.DLL" (ByVal Alignment As Long, ByVal BaseUnitY As Long, ByVal FontName As String, ByVal FontStyle As Long, ByVal FontDotSize As Long, ByVal TextData As String, ByVal ReversePrint As Long) As Long
'字體樣式及坐標
Private Declare Function PrintTTFXY Lib "LKPOSTOT.DLL" (ByVal BaseUnitX As Long, ByVal BaseUnitY As Long, ByVal FontName As String, ByVal FontStyle As Long, ByVal FontDotSize As Long, ByVal TextData As String, ByVal ReversePrint As Long) As Long
'條碼
Private Declare Function PrintBarCode Lib "LKPOSTOT.DLL" (ByVal Data As String, ByVal Symbology As Long, ByVal Height As Long, ByVal Width As Long, ByVal Alignment As Long, ByVal TextPosition As Long) As Long
'2維條碼
Private Declare Function PrintQRCodeAlign Lib "LKPOSTOT.DLL" (ByVal Alignment As Long, ByVal BaseUnitY As Long, ByVal Data As String, ByVal Size As Long, ByVal ModuleSize As Long, ByVal ECLevel As Long, ByVal Version As Long, ByVal MaskPattern As Long) As Long
'2維條碼及坐標
Private Declare Function PrintQRCodeXY Lib "LKPOSTOT.DLL" (ByVal BaseUnitX As Long, ByVal BaseUnitY As Long, ByVal Data As String, ByVal Size As Long, ByVal ModuleSize As Long, ByVal ECLevel As Long, ByVal Version As Long, ByVal MaskPattern As Long) As Long
'正常打印
Private Declare Function PrintNormal Lib "LKPOSTOT.DLL" (ByVal Data As String) As Long
'對齊打印圖
Private Declare Function PrintBitmapAlign Lib "LKPOSTOT.DLL" (ByVal Alignment As Long, ByVal BaseUnitY As Long, ByVal BitmapFile As Byte) As Long

Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Public Declare Function OpenDrawer Lib "LKPOSTOT.DLL" (ByVal DrawerPinNum As Long, ByVal PulseOnTime As Long, ByVal PulseOffTime As Long) As Long

Private Declare Function PrintBitmap Lib "LKPOSTOT.DLL" (ByVal BitmapName As String, ByVal Alignment As Long, ByVal Options As Long, ByVal Brightness As Long, ByVal ImageMode As Long) As Long

'儲存前半段跟後半段QRCODE
Dim strQR(1) As String

Public Const LK_SUCCESS = 0
Public Const LK_CREATE_ERROR = 1
Public Const LK_NOT_OPENED = 2
'QRCode(1營業人2非營業人,
'            銷售單號,
'            發票號碼,
'            發票金額,
'            發票日期,
'            發票抬頭,
'            發票電話,
'            發票地址,
'            隨機碼,
'            該店統編,
'            對象統編,
'            列印次數)
'20170712
Public Function QRCodeA(ByVal strSortFlag As String, _
                        ByVal strQRKey As String, _
                        ByVal strSellID As String, _
                        ByVal strIID As String, _
                        ByVal DMey As Double, _
                        ByVal strIDate As String, _
                        ByVal strIName As String, _
                        ByVal strITel As String, _
                        ByVal strIAddr, _
                        ByVal strPCKey As String, _
                        ByVal strUniteIDB As String, _
                        ByVal strUniteIDC As String, _
                        ByVal IPrintNum As Integer, _
                        ByVal strQR1 As String, _
                        ByVal TaxType As String, _
                        ByVal strDate As String, _
                        ByVal strITime As String, _
                        ByVal TotalAmount As Double, _
                        ByVal SalesAmount As Double, _
                        ByVal TaxAmount As Double, _
                        ByVal rsSellClone As ADODB.Recordset _
                        ) As String
    QRCodeA = QRCodePrintInUnite(strQRKey, strSellID, strIID, DMey, strIDate, strIName, strITel, strIAddr, strPCKey, strUniteIDB, strUniteIDC, IPrintNum, strQR1, TaxType, strDate, strITime, TotalAmount, SalesAmount, TaxAmount, rsSellClone)
End Function
Public Function UTF8Encode(ByVal szInput As String) As String
    
    Dim wch  As String
    Dim uch As String
    Dim szRet As String
    Dim x As Long
    Dim inputLen As Long
    Dim nAsc  As Long
    Dim nAsc2 As Long
    Dim nAsc3 As Long
    
    Dim LenTmp(2) As Long
    For x = 0 To 2
        LenTmp(x) = 0
    Next x
    
    If szInput = "" Then
        UTF8Encode = szInput
        Exit Function
    End If
    inputLen = Len(szInput)
    For x = 1 To inputLen
    '得到每個字符
        wch = Mid(szInput, x, 1)
        nAsc = AscW(wch)
        If nAsc < 0 Then nAsc = nAsc + 65536
        If (nAsc And &HFF80) = 0 Then
            szRet = szRet & wch
        Else
            If (nAsc And &HF000) = 0 Then
                uch = "%" & Hex(((nAsc \ 2 ^ 6)) Or &HC0) & Hex(nAsc And &H3F Or &H80)
                szRet = szRet & uch
               
            Else
                uch = "%" & Hex((nAsc \ 2 ^ 12) Or &HE0) & "%" & _
                Hex((nAsc \ 2 ^ 6) And &H3F Or &H80) & "%" & _
                Hex(nAsc And &H3F Or &H80)
                szRet = szRet & uch
            End If
        End If
        
        If Len(szRet) > 130 And (Len(szRet) - LenTmp(0)) <= 130 Then
            LenTmp(1) = Len(szRet)
        ElseIf Len(szRet) <= 130 Then
            LenTmp(0) = Len(szRet)
        End If
        
    Next

    UTF8Encode = Mid(szRet, 1, LenTmp(0))
End Function

Public Function GetQrCodeInv(ByVal InvoiceNumber As String, ByVal InvoiceDate As String, ByVal InvoiceTime As String, ByVal RandomNumber As String, ByVal SalesAmount As Long, ByVal TaxAmount As Long, ByVal TotalAmount As Long, ByVal BuyerIdentifier As String, ByVal RepresentIdentifier As String, ByVal SellerIdentifier As String, ByVal BusinessIdentifier As String, ByVal AESKey As String) As String
    Dim dblRetVal As Long

    If Dir(App.Path & "\ParaData.txt") <> "" Then Kill App.Path & "\ParaData.txt"
    If Dir(App.Path & "\GetData.txt") <> "" Then Kill App.Path & "\GetData.txt"
    If Dir(App.Path & "\GetQRCODE.bat") <> "" Then Kill App.Path & "\GetQRCODE.bat"
    
    Open App.Path & "\ParaData.txt" For Output Lock Write As #1
    Print #1, InvoiceNumber
    Print #1, InvoiceDate
    Print #1, InvoiceTime
    Print #1, RandomNumber
    Print #1, SalesAmount
    Print #1, TaxAmount
    Print #1, TotalAmount
    Print #1, BuyerIdentifier
    Print #1, RepresentIdentifier
    Print #1, SellerIdentifier
    Print #1, BusinessIdentifier
    Print #1, AESKey
    Close #1
    
    Open App.Path & "\GetQRCODE.bat" For Output Lock Write As #1
    Print #1, App.Path & "\QREncrypter.exe " & App.Path & "\ParaData.txt > " & App.Path & "\GetData.txt"
    Close #1
    
    dblRetVal = Shell(App.Path & "\GetQRCODE.bat", vbHide)
    While IsActive(dblRetVal)
        DoEvents
        Sleep (500)
    Wend
    
    If Dir(App.Path & "\GetData.txt") <> "" Then
        Open App.Path & "\GetData.txt" For Input As #1
        Line Input #1, GetQrCodeInv
        Close #1
    Else
        GetQrCodeInv = ""
    End If
    
'''''Print GetQrCodeInv("SZ80044985", "1020318", "151030", "3880", 0, 0, 10, "88888888", "11111111", "22222222", "23285582", "533EF81D5AEEC0CE25EC2A77CB8FDA33")
    '10碼
'''''SZ8004498510203183880000000000000000a8888888822222222rq8uVwFXvBFnOGL5RBOe2w==

End Function

Public Function IsActive(hProg As Long) As Long
    Dim hProc As Long, RetVal As Long
    
    Const PROCESS_QUERY_INFORMATION = &H400
    Const STILL_ACTIVE = 259

    hProc = OpenProcess(PROCESS_QUERY_INFORMATION, False, hProg)
    If hProc <> 0 Then
        GetExitCodeProcess hProc, RetVal
    End If
    IsActive = (RetVal = STILL_ACTIVE)
    CloseHandle hProc
End Function

Public Function QRCodePrintInUnite( _
                        ByVal strQRKey As String, _
                        ByVal strSellID As String, _
                        ByVal strIID As String, _
                        ByVal DMey As Double, _
                        ByVal strIDate As String, _
                        ByVal strIName As String, _
                        ByVal strITel As String, _
                        ByVal strIAddr, _
                        ByVal strPCKey As String, _
                        ByVal strUniteIDB As String, _
                        ByVal strUniteIDC As String, _
                        ByVal IPrintNum As Integer, _
                        ByVal strQR1 As String, _
                        ByVal TaxType As String, _
                        ByVal strDate As String, _
                        ByVal strITime As String, _
                        ByVal TotalAmount As Double, _
                        ByVal SalesAmount As Double, _
                        ByVal TaxAmount As Double, _
                        ByVal rsSellClone As ADODB.Recordset _
                        ) As String
    Dim nLen As Long
    Dim pbuf As String
    Dim ret As Long
    Dim Q As Integer
    Dim i As Integer
    Dim DMeyTmp As Double
    Dim strTmp As String
    Dim strTmp1 As String
    Dim DNum As Double
    Dim rsTmp As New ADODB.Recordset, rsTmp1 As New ADODB.Recordset, rsTmp2 As New ADODB.Recordset
    Dim rsSell  As New ADODB.Recordset
    Dim TMey As Double
    Dim iCount As Integer
'''    Dim TaxType As String
    Dim strStore As String
    Dim strFooter As String
'''    Dim strDate As String
        '20170517 壓碼 20170712 壓碼移到外面
'''    Dim SalesAmount As Double, TaxAmount As Double, TotalAmount As Double, strITime As String
    Open App.Path & "\" & SysDate & "_LOG.txt" For Append Lock Write As #1
        Print #1, Time & " 3開電子發票A"
    Close #1
    iCount = 0
    TMey = 0
'''    Set rsTmp = Nothing
'''    rsTmp.Open "Select * From EInvShipNoMain Where InvNum='" & strIID & "'", MyCn
'''    If rsTmp.RecordCount > 0 Then
'''        TaxType = "" & rsTmp!TaxType
'''        SalesAmount = rsTmp!SalesAmount
'''        TaxAmount = rsTmp!TaxAmount
'''        strITime = rsTmp!SysTime
'''        strDate = rsTmp!SysDate
'''        TotalAmount = rsTmp!TotalAmount
'''    End If
'    Set rsTmp2 = Nothing
'    rsTmp2.Open "Select * From Client Where ClientID='" & StoreNo & "'", MyCn
'    If rsTmp2.RecordCount > 0 Then
'    strStore = "" & rsTmp2!ClientName
'    End If
    strStore = Replace("" & Getini("機台設定", "門市名稱", App.Path & "\PovSQL.ini"), Chr(0), "")
    If strStore <> "Notfind" Then
        strStore = strStore
    Else
        strStore = ""
    End If
    strFooter = Replace("" & Getini("機台設定", "發票表尾", App.Path & "\PovSQL.ini"), Chr(0), "")
    If strFooter <> "Notfind" Then
        strFooter = strFooter
    Else
        strFooter = ""
    End If
    
    iCount = 0
    TMey = 0
'''    Set rsTmp = Nothing
'''    rsTmp.Open "Select * From EInvShipNoMain Where InvNum='" & strIID & "'", MyCn
'''    If rsTmp.RecordCount > 0 Then
'''        TaxType = "" & rsTmp!TaxType
'''    End If

    
    Set rsSell = Nothing
'''    rsSell.Open "Select * From EInvShipNoDet Where InvNum='" & strIID & "' Order by RowNum", MyCn
    Set rsSell = rsSellClone.Clone
    rsSell.Filter = "SellID='" & strSellID & "'"
    iCount = rsSell.RecordCount
    If iCount > 3 Then
        iCount = 3
    End If
    strQR(1) = Left(strQR(1) & Space(110), 110)
        
        Open App.Path & "\" & SysDate & "_LOG.txt" For Append Lock Write As #1
            Print #1, Time & " 3開電子發票0"
        Close #1
        

'    ret = OpenPort("USB", 38400)
'    ret = PrinterSts()
'    If ret <> LK_SUCCESS Then
'        QRCodePrintInUnite = "發票機開啟失敗！"
'        ret = ClosePort()
'        Exit Function
'    End If
'    ret = ClosePort()
'    ret = OpenPort("LK-TL120", 1)
    
    
    ret = OpenPort("LK-TL120", 1)
    If ret <> LK_SUCCESS Then
        QRCodePrintInUnite = "發票機開啟失敗！"
        Exit Function
    End If
    
        Open App.Path & "\" & SysDate & "_LOG.txt" For Append Lock Write As #1
            Print #1, Time & " 3開電子發票1"
        Close #1
    '20170517 壓碼
    If strUniteIDC = "" Then
        strUniteIDC = "0"
    End If
    '20170712 壓碼移到外面
'''    strQR1 = GetQrCodeInv(strIID, (Val(strIDate) - 19110000), strITime, strPCKey, SalesAmount, TaxAmount, TotalAmount, Format(strUniteIDC, "00000000"), "00000000", Format(strUniteIDB, "00000000"), Format(strUniteIDB, "00000000"), strQRKey)
    Open App.Path & "\" & SysDate & "_LOG.txt" For Append Lock Write As #1
        Print #1, Time & " 3開電子發票2"
    Close #1
    '這台印表機每英寸180dpi換算下來1mm=180/25.4=7.0866點

    ret = PrintStart()

    ret = SetMasterUnit(0)  '0以點為單位  1以mm為單位  2以cm為單位

    ret = PrintingWidth(404)   '規格訂定的紙寬為5.7cm  57*7.0866=404
    
'     ret = SetLabelSize(404, 220)   '標籤模式訂定大小 以座標來印(3.1)
     ret = SetLabelSize(404, 200)   '標籤模式訂定大小 以座標來印(3.1)
    ret = PrintBitmap("LKView.bmp", 0, 0, 10, 0)
    
'    ret = SetLabelSize(404, 304)   '標籤模式訂定大小 以座標來印
    
'    ret = PrintTTFXY(8, 0, "細明體", 3, 43, strIName, 0)
'    ret = PrintTTFXY(110, 26, "細明體", 3, 25, Mid(strIName, 12), 0)
    If IPrintNum = 0 Then
        ret = PrintTTFXY(35, 0, "細明體", 1, 38, "電子發票證明聯", 0)
    Else
        ret = PrintTTFXY(0, 0, "細明體", 1, 38, "電子發票證明聯", 0)
        ret = PrintTTFXY(270, 10, "細明體", 1, 28, "(補印)", 0)
    End If
    ret = PrintTTFXY(18, 35, "Arial", 1, 52, InvoiceMonthCode(strIDate), 0) '月份+30
    
    ret = PrintTTFXY(36, 82, "Arial", 1, 52, Left(strIID, 2) & "-" & Right(strIID, 8), 0) '發票號+30
    
    If IPrintNum = 0 Then
    strTmp1 = Format(Date, "YYYY-MM-DD") & " " & Format(Time, "HH:MM:SS")
    Else
'    strTmp1 = Format(strDate, "YYYY-MM-DD") & " " & Format(strITime, "HH:MM:SS")
    strTmp1 = Left(strDate, 4) & "-" & Mid(strDate, 5, 2) & "-" & Right(strDate, 2) & " " & Left(strITime, 2) & ":" & Mid(strITime, 3, 2) & ":" & Right(strITime, 2)
    End If
    
    If Len(strUniteIDC) > 1 Then
        strTmp1 = strTmp1 & "   格式 25  "
    End If
    ret = PrintTTFXY(14, 125, "細明體", 1, 22, strTmp1, 0) '日期
    ret = PrintTTFXY(14, 147, "細明體", 1, 22, "隨機碼  " & strPCKey & "     總計" & DMey & "", 0) '隨機碼
    If strUniteIDB = "" Then strUniteIDB = "0"
    If Len(strUniteIDC) <= 1 Then
        strUniteIDC = "0"
        strTmp1 = ""
    Else
        strTmp1 = "買方 " & Format(strUniteIDC, "00000000") & ""
    End If
    ret = PrintTTFXY(14, 169, "細明體", 1, 22, "賣方 " & Format(strUniteIDB, "00000000") & "    " & strTmp1 & "", 0)
        Open App.Path & "\" & SysDate & "_LOG.txt" For Append Lock Write As #1
            Print #1, Time & " 3開電子發票3"
        Close #1
    
    ret = PrintLabel()
    DoEvents
    ret = PrintBarCode(Left((Val(Left(strIDate, 4)) - 1911), 3) & Format(Int((Mid(strIDate, 5, 2) + 1) / 2) * 2, "00") & strIID & strPCKey, 109, 40, 404, 0, 0)
    strTmp1 = ""
    strTmp = ""
    strTmp = strTmp & strQR1
'    strTmp = strTmp & strIID                    '發票編號
'    strTmp = strTmp & Val(strIDate) - 19110000  '發票日期(民國年)
'    strTmp = strTmp & strPCKey                  '4碼隨機碼
'    strTmp = strTmp & Format(0, "00000000")     '銷售額(8碼)
'    strTmp = strTmp & Format(DMey, "00000000")  '總計額(8碼)
    
'    strTmp = strTmp & Format(strUniteIDC, "00000000") '買方統編
'    strTmp = strTmp & Format(strUniteIDB, "00000000") '賣方統編
'    strTmp = strTmp & strQRKey                        '32碼
    strTmp = strTmp & ":"
    strTmp = strTmp & "**********"              '提供營業人自行放置所需資訊，若不使用則以10個“*”符號呈現。
    strTmp = strTmp & ":"
    strTmp = strTmp & iCount                    '二維條碼記載完整品目筆數
    strTmp = strTmp & ":"
    strTmp = strTmp & rsSell.RecordCount        '該張發票交易品目總筆數
    strTmp = strTmp & ":"
    strTmp = strTmp & "1"                       '中文編碼參數 UTF-8
    strTmp = strTmp & ":"
    
    '接續之品名、數量、單價為重覆循環呈現至所有品目記載完成，
    '若品目筆數過多以致左右兩個二維條碼無法全部記載，
    '則以記載最多可放置於左右兩個二維條碼內容之品目為原則。
'''    Set rsTmp = rsSell.Clone
'''    rsTmp.MoveFirst
'''    While Not rsTmp.EOF
'''        If rsTmp.AbsolutePosition = 1 Then
'''            strTmp = strTmp & rsTmp!Description
'''            strTmp = strTmp & ":"
'''            strTmp = strTmp & Val("" & rsTmp!Quantity)
'''            strTmp = strTmp & ":"
'''            strTmp = strTmp & Val("" & rsTmp!Unitprice)
'''        ElseIf rsTmp.AbsolutePosition <= iCount Then
'''            strTmp1 = strTmp1 & rsTmp!Description
'''            strTmp1 = strTmp1 & ":"
'''            strTmp1 = strTmp1 & Val("" & rsTmp!Quantity)
'''            strTmp1 = strTmp1 & ":"
'''            strTmp1 = strTmp1 & Val("" & rsTmp!Unitprice)
'''        End If
'''
'''        rsTmp.MoveNext
'''    Wend
    Dim InvMey As Double, DTmp As Double, DTmp1 As Double
    
    rsSell.MoveFirst
    i = 1
    While Not rsSell.EOF
        DNum = Val("" & rsSell!TotalSellNum)
        If DNum = 0 Then
            DNum = 1
        End If
        If rsSell!SellMode = "2" Then
            DTmp1 = -1
        ElseIf rsSell!SellMode = "8" Then
            DTmp1 = -1
        Else
            DTmp1 = 1
        End If
        InvMey = DTmp1 * (rsSell!SellCash + rsSell!SellCardAmount)
        DTmp = Format(Val(InvMey) / DNum, "0.00")
        If i = 1 Then
            strTmp = strTmp & rsSell!GoodID
            strTmp = strTmp & ":"
            strTmp = strTmp & DNum
            strTmp = strTmp & ":"
            strTmp = strTmp & DTmp
        ElseIf i <= iCount Then
            strTmp1 = strTmp1 & rsSell!GoodID
            strTmp1 = strTmp1 & ":"
            strTmp1 = strTmp1 & DNum
            strTmp1 = strTmp1 & ":"
            strTmp1 = strTmp1 & DTmp
        End If
        i = i + 1
        rsSell.MoveNext
    Wend
    For i = 0 To 1
        strQR(i) = ""
    Next i
    'UTF8Encode(strTmp)長度不得超過130
    strQR(0) = UTF8Encode(strTmp)
    strQR(1) = "**"
    If strTmp1 <> "" Then
        strQR(1) = strQR(1) & UTF8Encode(strTmp1)
    End If
'''    If Len(strQR(1)) < 110 Then
'''        strQR(1) = Left(strQR(1) & Space(110), 110)
'''    End If
    strQR(1) = Left(strQR(1) & Space(110), 110)
        Open App.Path & "\" & SysDate & "_LOG.txt" For Append Lock Write As #1
            Print #1, Time & " 3開電子發票4"
        Close #1
    '這裡印條碼的高度以點來算是對的，但以mm為單位的話卻不知算法為何
    ret = SetLabelSize(404, 180)
    ret = PrintQRCodeXY(14, 10, strQR(0), 0, 3, 0, 6, -1)
    ret = PrintQRCodeXY(219, 10, strQR(1), 0, 3, 0, 6, -1)
    ret = PrintTTFXY(14, 138, "細明體", 1, 20, Left("門市  " & strStore & Space(10), 10) & "TEL " & strITel, 0)
'    ret = PrintTTFXY(14, 138, "細明體", 2, 20, "門市  " & strStore, 0)
    ret = PrintTTFXY(14, 160, "細明體", 1, 20, "" & strFooter, 0)
    
'''    ret = PrintQRCodeXY(14, 35, UTF8Encode(strTmp), 0, 3, 0, 6, -1)
'''    ret = PrintQRCodeXY(234, 35, UTF8Encode(strTmp), 0, 3, 0, 0, -1)
    ret = PrintLabel()
    DoEvents
'''    ret = PrintTTFXY(14, 20, "細明體", 1, 20, "門市  " & strStore, 0)
    
    Dim strArrTmp(2) As String
    For i = 0 To 2
        strArrTmp(i) = ""
    Next i
    
    If Len(strUniteIDC) > 1 Then
        ret = SetLabelSize(404, 20)
        ret = PrintLabel()
        DoEvents
'        TMey = 0
'        Set rsTmp = rsSell.Clone
'        rsTmp.MoveFirst
''''        ret = PrintNormal("" & vbNewLine)
''''        ret = PrintNormal("" & vbNewLine)
        ret = PrintNormal("" & vbNewLine)
        ret = PrintNormal("-----------------------------" & vbNewLine)
'        ret = PrintNormal("運動用品" & vbNewLine)
'        While Not rsTmp.EOF
'            strArrTmp(0) = "$"
''            ret = PrintNormal(rsTmp!Description & "  $" & Format(rsTmp!Amount, "0.00") & " " & IIf(TaxType = "1", "TX", IIf(TaxType = "2", "TZ", "")) & "" & vbNewLine)
'            ret = PrintNormal(Left(rsTmp!Description & Space(13), 13) & "  " & Right(Space(10) & "$" & Format(rsTmp!Amount, "0.00") & " " & IIf(TaxType = "1", "TX", IIf(TaxType = "2", "TZ", "")) & "", 14) & vbNewLine)
'            TMey = TMey + Val("" & rsTmp!Amount)
'            rsTmp.MoveNext
'        Wend
'        If TaxType = "1" Then
'            strArrTmp(1) = Format(TMey / 1.05, "0")
'        Else
'            strArrTmp(1) = TMey
'        End If
'        strArrTmp(1) = Format(TMey / 1.05, "0")
'        strArrTmp(2) = TMey
'        strArrTmp(0) = strArrTmp(2) - strArrTmp(1)
'        For i = 0 To 2
'            strArrTmp(i) = "$" & strArrTmp(i)
'        Next i
'        For i = 0 To 2
'            strArrTmp(i) = Right(Space(6) & strArrTmp(i), 6)
'        Next i
'        ret = PrintNormal("應稅合計" & Space(9) & strArrTmp(1) & ".00 " & IIf(TaxType = "1", "TX", IIf(TaxType = "2", "TZ", "")) & "" & vbNewLine)
'        ret = PrintNormal("" & IIf(TaxType = "1", "稅金(5%)", IIf(TaxType = "2", "零稅(0%)", "免稅(0%)")) & "" & Space(9) & strArrTmp(0) & ".00   " & vbNewLine)
'        ret = PrintNormal("-----------------------------" & vbNewLine)
'        ret = PrintNormal("合計金額" & strArrTmp(2) & ".00   " & vbNewLine)
    Else
        ret = PrintNormal(Chr(27) & "|fP")
    End If
    
        Open App.Path & "\" & SysDate & "_LOG.txt" For Append Lock Write As #1
            Print #1, Time & " 3開電子發票5"
        Close #1
    

    ret = PrintStop()

    ret = ClosePort()
    If ret <> LK_SUCCESS Then
        QRCodePrintInUnite = "發票機關閉失敗，請檢查發票機！"
        Exit Function
    End If

End Function

'Public Sub QRCodePrintNoUnite(ByVal rsSell As ADODB.Recordset, _
'                        ByVal strIID As String, _
'                        ByVal DMey As Double, _
'                        ByVal strIDate As String, _
'                        ByVal strIName As String, _
'                        ByVal strITel As String, _
'                        ByVal strIAddr, _
'                        ByVal strPCKey As String, _
'                        ByVal strUniteIDB As String, _
'                        ByVal strUniteIDC As String, _
'                        ByVal IPrintNum As Integer)
'
'End Sub


Public Function InvoiceMonthCode(ByVal strIDate As String) As String
    strIDate = Left(strIDate, 6)
    InvoiceMonthCode = str(Val(Left(strIDate, 4)) - 1911) & "年"
    Select Case Right(strIDate, 2)
        Case "01", "02"
            InvoiceMonthCode = InvoiceMonthCode & "01-02月"
        Case "03", "04"
            InvoiceMonthCode = InvoiceMonthCode & "03-04月"
        Case "05", "06"
            InvoiceMonthCode = InvoiceMonthCode & "05-06月"
        Case "07", "08"
            InvoiceMonthCode = InvoiceMonthCode & "07-08月"
        Case "09", "10"
            InvoiceMonthCode = InvoiceMonthCode & "09-10月"
        Case "11", "12"
            InvoiceMonthCode = InvoiceMonthCode & "11-12月"
    End Select
End Function
Public Function QRCodePrint(ByVal strIID As String) As String
    Dim nLen As Long
    Dim pbuf As String
    Dim ret As Long
    Dim Q As Integer
    Dim i As Integer
    Dim DMeyTmp As Double
    Dim strTmp As String
    Dim strTmp1 As String
    Dim DNum As Double
    Dim rsTmp As New ADODB.Recordset, rsTmp1 As New ADODB.Recordset
    Dim rsSell  As New ADODB.Recordset
    Dim TMey As Double
    Dim iCount As Integer
    Dim TaxType As String
    Dim strSellID As String
    Dim DMey As Double
    Dim strIDate As String
    Dim strPCKey As String
    Dim strUniteIDB As String
    Dim strUniteIDC As String
    Dim strQRKey As String
    Dim strAPPID As String
    Dim strClientID As String
    iCount = 0
    TMey = 0
    Set rsTmp = Nothing
    rsTmp.Open "Select * From EInvShipNoMainTransition Where InvNum='" & strIID & "'", MyCn
    If rsTmp.RecordCount > 0 Then
        TaxType = "" & rsTmp!TaxType
        strSellID = "" & rsTmp!PosOrderNo
        DMey = Val("" & rsTmp!TotalAmount)
        strIDate = "" & rsTmp!InvDate
        strPCKey = "" & rsTmp!RandomNum
        strUniteIDC = "" & rsTmp!CustomerIdentifier
        strAPPID = "" & rsTmp!AppID
    End If
    Set rsTmp = Nothing
    rsTmp.Open "Select * From EInvSet Where AppID='" & strAPPID & "'", MyCn
    If rsTmp.RecordCount > 0 Then
        strQRKey = "" & rsTmp!qrApiKey
        strClientID = "" & rsTmp!ClientID
    End If
    
    Set rsTmp = Nothing
    rsTmp.Open "Select * From Client Where ClientID='" & strClientID & "'", MyCn
    If rsTmp.RecordCount > 0 Then
        strUniteIDB = "" & rsTmp!UniteID
    End If
    
    Set rsSell = Nothing
    rsSell.Open "Select * From EInvShipNoDetTransition Where InvNum='" & strIID & "' Order by RowNum", MyCn
    iCount = rsSell.RecordCount
    If iCount > 3 Then
        iCount = 3
    End If
'    ret = OpenPort("USB", 38400)
    ret = OpenPort("USB", 38400)
    ret = PrinterSts()
    If ret <> LK_SUCCESS Then
        QRCodePrint = "發票機開啟失敗！"
        Exit Function
    End If
    ret = ClosePort()

    '這台印表機每英寸180dpi換算下來1mm=180/25.4=7.0866點

    ret = PrintStart()

    ret = SetMasterUnit(0)  '0以點為單位  1以mm為單位  2以cm為單位

    ret = PrintingWidth(404)   '規格訂定的紙寬為5.7cm  57*7.0866=404
    
    ret = SetLabelSize(404, 235)   '標籤模式訂定大小 以座標來印
    ret = PrintBitmap("LKView.bmp", 0, 0, 3, 0)
'''    ret = PrintTTFXY(32, 0, "細明體", 3, 25, Left(strIName, 11), 0)
'''    ret = PrintTTFXY(110, 26, "細明體", 3, 25, Mid(strIName, 12), 0)
    ret = PrintTTFXY(50, 0, "細明體", 2, 36, "電子發票證明聯", 0)
    
    ret = PrintTTFXY(0, 35, "Arial", 2, 66, InvoiceMonthCode(strIDate), 0)
    
    ret = PrintTTFXY(7, 99, "Arial", 2, 66, Left(strIID, 2) & "-" & Right(strIID, 8), 0)
    strTmp1 = Format(Date, "YYYY-MM-DD") & " " & Format(Time, "HH:MM:SS")
    If Len(strUniteIDC) > 1 Then
        strTmp1 = strTmp1 & "格式 25"
    End If
    ret = PrintTTFXY(14, 163, "細明體", 1, 22, strTmp1, 0)
    ret = PrintTTFXY(14, 185, "細明體", 1, 22, "隨機碼  " & strPCKey & "     總計" & DMey & "", 0)
    If strUniteIDB = "" Then strUniteIDB = "0"
    If strUniteIDC = "" Then
        strUniteIDC = "0"
        strTmp1 = ""
    Else
        strTmp1 = "買方 " & Format(strUniteIDC, "00000000") & ""
    End If
    ret = PrintTTFXY(14, 207, "細明體", 1, 22, "賣方 " & Format(strUniteIDB, "00000000") & "    " & strTmp1 & "", 0)
    ret = PrintLabel()
    ret = PrintBarCode(Left((Val(Left(strIDate, 4)) - 1911), 3) & Format(Int((Mid(strIDate, 5, 2) + 1) / 2) * 2, "00") & strIID & strPCKey, 109, 40, 404, 0, 0)
    strTmp1 = ""
    strTmp = ""
    strTmp = strTmp & strIID                    '發票編號
    strTmp = strTmp & Val(strIDate) - 19110000  '發票日期(民國年)
    strTmp = strTmp & strPCKey                  '4碼隨機碼
    strTmp = strTmp & Format(0, "00000000")     '銷售額(8碼)
    strTmp = strTmp & Format(DMey, "00000000")  '總計額(8碼)
    
    strTmp = strTmp & Format(strUniteIDC, "00000000") '買方統編
    strTmp = strTmp & Format(strUniteIDB, "00000000") '賣方統編
    strTmp = strTmp & strQRKey                        '32碼
    strTmp = strTmp & ":"
    strTmp = strTmp & "**********"              '提供營業人自行放置所需資訊，若不使用則以10個“*”符號呈現。
    strTmp = strTmp & ":"
    strTmp = strTmp & iCount                    '二維條碼記載完整品目筆數
    strTmp = strTmp & ":"
    strTmp = strTmp & rsSell.RecordCount        '該張發票交易品目總筆數
    strTmp = strTmp & ":"
    strTmp = strTmp & "1"                       '中文編碼參數 UTF-8
    strTmp = strTmp & ":"
    
    '接續之品名、數量、單價為重覆循環呈現至所有品目記載完成，
    '若品目筆數過多以致左右兩個二維條碼無法全部記載，
    '則以記載最多可放置於左右兩個二維條碼內容之品目為原則。
    Set rsTmp = rsSell.Clone
    rsTmp.MoveFirst
    While Not rsTmp.EOF
        If rsTmp.AbsolutePosition = 1 Then
            strTmp = strTmp & rsTmp!Description
            strTmp = strTmp & ":"
            strTmp = strTmp & Val("" & rsTmp!Quantity)
            strTmp = strTmp & ":"
            strTmp = strTmp & Val("" & rsTmp!Unitprice)
        ElseIf rsTmp.AbsolutePosition <= iCount Then
            strTmp1 = strTmp1 & rsTmp!Description
            strTmp1 = strTmp1 & ":"
            strTmp1 = strTmp1 & Val("" & rsTmp!Quantity)
            strTmp1 = strTmp1 & ":"
            strTmp1 = strTmp1 & Val("" & rsTmp!Unitprice)
        End If
        
        rsTmp.MoveNext
    Wend
    For i = 0 To 1
        strQR(i) = ""
    Next i
    'UTF8Encode(strTmp)長度不得超過130
    strQR(0) = UTF8Encode(strTmp)
    strQR(1) = "**"
    If strTmp1 <> "" Then
        strQR(1) = strQR(1) & UTF8Encode(strTmp1)
    End If
'''    If Len(strQR(1)) < 110 Then
'''        strQR(1) = Left(strQR(1) & Space(110), 110)
'''    End If
    strQR(1) = Left(strQR(1) & Space(110), 110)
    '這裡印條碼的高度以點來算是對的，但以mm為單位的話卻不知算法為何
    ret = SetLabelSize(404, 145)
    ret = PrintQRCodeXY(14, 20, strQR(0), 0, 3, 0, 6, -1)
    ret = PrintQRCodeXY(234, 20, strQR(1), 0, 3, 0, 0, -1)
'''    ret = PrintQRCodeXY(14, 35, UTF8Encode(strTmp), 0, 3, 0, 6, -1)
'''    ret = PrintQRCodeXY(234, 35, UTF8Encode(strTmp), 0, 3, 0, 0, -1)
    ret = PrintLabel()
    Dim strArrTmp(2) As String
    For i = 0 To 2
        strArrTmp(i) = ""
    Next i
    
    If Len(strUniteIDC) > 1 Then
        ret = SetLabelSize(404, 20)
        ret = PrintLabel()
        TMey = 0
        Set rsTmp = rsSell.Clone
        rsTmp.MoveFirst
'''        ret = PrintNormal("" & vbNewLine)
'''        ret = PrintNormal("" & vbNewLine)
        ret = PrintNormal("" & vbNewLine)
        ret = PrintNormal("-----------------------------" & vbNewLine)
        While Not rsTmp.EOF
            strArrTmp(0) = "$"
            ret = PrintNormal(rsTmp!Description & "  $" & Format(rsTmp!Amount, "0.00") & " " & IIf(TaxType = "1", "TX", IIf(TaxType = "2", "TZ", "")) & "" & vbNewLine)
            TMey = TMey + Val("" & rsTmp!Amount)
            rsTmp.MoveNext
        Wend
        If TaxType = "1" Then
            strArrTmp(1) = Format(TMey / 1.05, "0")
        Else
            strArrTmp(1) = TMey
        End If
        strArrTmp(1) = Format(TMey / 1.05, "0")
        strArrTmp(2) = TMey
        strArrTmp(0) = strArrTmp(2) - strArrTmp(1)
        For i = 0 To 2
            strArrTmp(i) = "$" & strArrTmp(i)
        Next i
        For i = 0 To 2
            strArrTmp(i) = Right(Space(6) & strArrTmp(i), 6)
        Next i
        ret = PrintNormal("應稅合計" & Space(9) & strArrTmp(1) & ".00 " & IIf(TaxType = "1", "TX", IIf(TaxType = "2", "TZ", "")) & "" & vbNewLine)
        ret = PrintNormal("" & IIf(TaxType = "1", "稅金(5%)", IIf(TaxType = "2", "零稅(0%)", "免稅(0%)")) & "" & Space(9) & strArrTmp(0) & ".00   " & vbNewLine)
        ret = PrintNormal("-----------------------------" & vbNewLine)
        ret = PrintNormal("合計金額" & strArrTmp(2) & ".00   " & vbNewLine)
    End If
    ret = PrintNormal(Chr(27) & "|fP")
    
    

    ret = PrintStop()

    ret = ClosePort()
    If ret <> LK_SUCCESS Then
        QRCodePrint = "發票機關閉失敗，請檢查發票機！"
        Exit Function
    End If

End Function

Public Function QRAllowancePrint(ByVal strIID As String, ByVal InvNum As String, ByVal OutUniteID As String, ByVal OutUniteName As String) As String
    Dim nLen As Long
    Dim pbuf As String
    Dim ret As Long
    Dim Q As Integer
    Dim i As Integer
    Dim rsTmp As New ADODB.Recordset
    Dim rsDet As New ADODB.Recordset
    Dim strSQL As String
    Dim InUniteID As String
    Dim InUniteName As String
    Dim InvDate As String
    Dim strTmp As String
    Dim ArrDTmp(3) As Double
    Dim stInvoiceName As String, TaxType As String
    Set rsTmp = Nothing
    rsTmp.Open "Select * From EInvAllowanceMain Where SerialID='" & strIID & "'", MyCn
    If rsTmp.RecordCount > 0 Then
        InUniteID = "" & rsTmp!CustomerIdentifier
        InUniteName = "" & rsTmp!CustomerName
    End If
    
    
    Set rsTmp = Nothing
    rsTmp.Open " Select top 1 TaxType From EInvAllowanceDet Where SerialID='" & strIID & "'", MyCn
    If rsTmp.RecordCount > 0 Then
        TaxType = "" & rsTmp!TaxType
    End If
    
    
    Set rsDet = Nothing
    rsDet.Open "Select * From EInvAllowanceDet Where InvNum='" & InvNum & "' And SerialID='" & strIID & "' Order by Item", MyCn
    If rsDet.RecordCount > 0 Then
        InvDate = "" & rsDet!InvDate
    End If

'    ret = OpenPort("USB", 38400)
'    ret = PrinterSts()
'    If ret <> LK_SUCCESS Then
'        QRAllowancePrint = "發票機開啟失敗！"
'        ret = ClosePort()
'        Exit Function
'    End If
'    ret = ClosePort()
'    ret = OpenPort("LK-TL120", 1)

    ret = OpenPort("LK-TL120", 1)
    If ret <> LK_SUCCESS Then
        QRAllowancePrint = "發票機開啟失敗！"
        Exit Function
    End If


    '這台印表機每英寸180dpi換算下來1mm=180/25.4=7.0866點

    ret = PrintStart()

    ret = SetMasterUnit(0)  '0以點為單位  1以mm為單位  2以cm為單位

    ret = PrintingWidth(404)   '規格訂定的紙寬為5.7cm  57*7.0866=404
    '20161229 LOG圖暫時停用
'''    ret = PrintBitmap("LKView.bmp", 0, 0, 3, 0)
    stInvoiceName = Getini("機台設定", "發票抬頭", App.Path & "\PovSQL.ini")
    If stInvoiceName <> "Notfind" And Len(stInvoiceName) > 0 Then
        stInvoiceName = Replace(Trim(stInvoiceName), Chr(0), "")
    Else
        stInvoiceName = ""
    End If
    ret = SetLabelSize(404, 68)
    ret = PrintTTFXY(32, 0, "細明體", 3, 25, Left(stInvoiceName, 12), 0)
    ret = PrintTTFXY(110, 26, "細明體", 3, 25, Mid(stInvoiceName, 13), 0)
    ret = PrintLabel()
    ret = SetLabelSize(404, 454)   '標籤模式訂定大小 以座標來印
    ret = PrintTTFXY(-2, 50, "細明體", 2, 26, "電子發票銷貨退回，進貨退出或", 0)
    ret = PrintTTFXY(76, 80, "細明體", 2, 26, "折讓證明單證明聯", 0)
    
    ret = PrintTTFXY(115, 149, "Arial", 2, 30, Format(Date, "YYYY-MM-DD"), 0)
    strTmp = "賣方統編:" & OutUniteID
    ret = PrintTTFXY(8, 213, "細明體", 1, 26, strTmp, 0)
    
    If Len(OutUniteName) > 9 Then
        strTmp = "賣方名稱:" & Left(OutUniteName, 8)
        ret = PrintTTFXY(8, 245, "細明體", 1, 26, strTmp, 0)
        
        strTmp = Mid(OutUniteName, 9)
        ret = PrintTTFXY(8, 277, "細明體", 1, 26, strTmp, 0)
        
        
        strTmp = "發票開立日期:" & InvDate
        ret = PrintTTFXY(8, 309, "細明體", 1, 26, strTmp, 0)
        ret = PrintTTFXY(8, 341, "細明體", 1, 26, InvNum, 0)
        
        strTmp = "買方統編:" & InUniteID
        ret = PrintTTFXY(8, 373, "細明體", 1, 26, strTmp, 0)
        
        strTmp = "買方名稱:" & InUniteName
        ret = PrintTTFXY(8, 405, "細明體", 1, 26, strTmp, 0)
    
    
    Else
        strTmp = "賣方名稱:" & OutUniteName
        ret = PrintTTFXY(8, 245, "細明體", 1, 26, strTmp, 0)
        
        strTmp = "發票開立日期:" & InvDate
        ret = PrintTTFXY(8, 277, "細明體", 1, 26, strTmp, 0)
        ret = PrintTTFXY(8, 309, "細明體", 1, 26, InvNum, 0)
        
        strTmp = "買方統編:" & InUniteID
        ret = PrintTTFXY(8, 341, "細明體", 1, 26, strTmp, 0)
        
        strTmp = "買方名稱:" & InUniteName
        ret = PrintTTFXY(8, 373, "細明體", 1, 26, strTmp, 0)
    End If
    
    
    ret = PrintLabel()
    
    For i = 0 To 3
        ArrDTmp(i) = 0
    Next
    If rsDet.RecordCount > 0 Then
        ret = SetLabelSize(404, 20)
        ret = PrintLabel()
        ret = PrintNormal("-----------------------------" & vbNewLine)
        TMey = 0
        rsDet.MoveFirst
        While Not rsDet.EOF
            strTmp = ""
            strTmp = strTmp & rsDet!Description
            strTmp = strTmp & ":"
            strTmp = strTmp & Val("" & rsDet!Quantity)
            strTmp = strTmp & ":"
            strTmp = strTmp & Val("" & rsDet!Unitprice)
            strTmp = strTmp & ":"
            strTmp = strTmp & Val("" & rsDet!Amount)
            ret = PrintNormal(strTmp & vbNewLine)
            ArrDTmp(0) = ArrDTmp(0) + Val("" & rsDet!Tax)
            ArrDTmp(1) = ArrDTmp(1) + Val("" & rsDet!Amount)
            rsDet.MoveNext
        Wend
'        strTmp = "課稅別:TX"
        strTmp = "課稅別:" & IIf(TaxType = "1", "TX", IIf(TaxType = "3", "TZ", ""))
        ret = PrintNormal(strTmp & vbNewLine)
        
        strTmp = "營業稅額:" & ArrDTmp(0)
        ret = PrintNormal(strTmp & vbNewLine)
        
        strTmp = "金額:" & ArrDTmp(1)
        ret = PrintNormal(strTmp & vbNewLine)
        
    End If
    strTmp = "簽收人:"
    ret = PrintNormal(strTmp & vbNewLine)
    ret = PrintNormal(Chr(27) & "|fP")
    

'''    ret = PrintStop()

    ret = ClosePort()
    If ret <> LK_SUCCESS Then
        QRAllowancePrint = "發票機關閉失敗，請檢查發票機！"
        Exit Function
    End If

End Function


Public Function CutLK120() As String
    Dim nLen As Long
    Dim pbuf As String
    Dim ret As Long

'    ret = OpenPort("USB", 38400)
'    ret = PrinterSts()
'    If ret <> LK_SUCCESS Then
'        CutLK120 = "發票機開啟失敗！"
'        ret = ClosePort()
'        Exit Function
'    End If
'    ret = ClosePort()
'    ret = OpenPort("LK-TL120", 1)

    ret = OpenPort("LK-TL120", 1)
    If ret <> LK_SUCCESS Then
        CutLK120 = "發票機開啟失敗！"
        Exit Function
    End If

    '這台印表機每英寸180dpi換算下來1mm=180/25.4=7.0866點

    ret = PrintStart()

    ret = SetMasterUnit(0)  '0以點為單位  1以mm為單位  2以cm為單位

    ret = PrintingWidth(404)   '規格訂定的紙寬為5.7cm  57*7.0866=404
    ret = PrintNormal(Chr(27) & "|fP")
    

    ret = PrintStop()

    ret = ClosePort()
    If ret <> LK_SUCCESS Then
        CutLK120 = "發票機關閉失敗，請檢查發票機！"
        Exit Function
    End If
End Function
